Czech CC0 dictionaries
======================

This repository contains:
- Czech spell check dictionary (Hunspell),
- tools to create it from the [Czech morphological dictionary](https://github.com/plin/slovnik)
and [Czech Wikidata lexemes](https://www.wikidata.org/wiki/Wikidata:Lexicographical_data),
- extensions to use it in the [LibreOffice](https://www.libreoffice.org/) suite,
[Firefox](https://www.mozilla.org/firefox/) or [Thunderbird](https://www.thunderbird.net/).

The dictionary is considered as **experimental** (many words missing),
with a lot of space for improvements.

For online check of words, more details and instructions how to contribute,
see the webpage [ceskeslovniky.cz](http://ceskeslovniky.cz).

Installation of the LibreOffice extension
-----------------------------------------
This dictionary is considered **experimental**. For common use please install Czech dictionaries
from [extensions.libreoffice.org](https://extensions.libreoffice.org/en/extensions/show/czech-dictionaries).
1. To get the extension package:
    - run `make extensions` or `make extensions_in_podman` to build the extension into `dist`
    - or download the extension file from the latest [GitLab build artifacts](https://gitlab.com/strepon/czech-cc0-dictionaries/-/pipelines?ref=master)
    - or download the extension file from the [LibreOffice extension page](https://extensions.libreoffice.org/extensions/czech-cc0-dictionaries-ceske-cc0-slovniky)
1. In LibreOffice, choose `Tools - Extension Manager`, click the `Add` button
and select the `.oxt` file.

Installation of the Mozilla extension
-------------------------------------
This dictionary is considered **experimental**. For common use please install Czech dictionaries
from [addons.mozilla.org](https://addons.mozilla.org/firefox/addon/czech-spell-checking-dictionar/).
1. To get the extension package:
    - run `make extensions` or `make extensions_in_podman` to build the extension into `dist`
    - or download the extension file from the latest [GitLab build artifacts](https://gitlab.com/strepon/czech-cc0-dictionaries/-/pipelines?ref=master)
    - or download and install the extension from the official add-on servers for [Firefox](https://addons.mozilla.org/firefox/addon/czech-spellcheck-cc0/), [Thunderbird](https://addons.thunderbird.net/thunderbird/addon/czech-spellcheck-cc0/) or [SeaMonkey](https://addons.thunderbird.net/seamonkey/addon/czech-spellcheck-cc0/)
1. In Firefox/Thunderbird, open the add-ons manager (`"hamburger button" - Add-ons`).
1. Click the *gear* button in the top right corner.
1. Select *Debug Add-ons*.
1. Click *Load Temporary Add-on* button.
1. In file picker select the `.xpi` file built above.
1. After application restart, the extension will be uninstalled again.

Adding new words
----------------
As the dictionary uses Wikidata lexemes, new words and their forms can be
easily added by adding them there:

1. Make sure that the word to be added is not already contained in Wikidata,
e.g. by searching at the [Ordia tools page](https://tools.wmflabs.org/ordia/)
(left top corner).
1. Create a lexeme at the [New Lexeme page](https://www.wikidata.org/wiki/Special:NewLexeme),
or use [friendlier templates](https://tools.wmflabs.org/lexeme-forms/) for some Czech parts of speech.
If a new form for an already existing lemma is needed, edit the existing lexeme page.

Creating Hunspell dictionary
----------------------------
1. Have Python 3, Hunspell (+ munch tool) and git (+ git-lfs) installed on your system.
    - On Debian-based system, you can install them all by running `make prepare_debian`.
1. Run `make prepare_python` to install required Python dependencies.
1. Run `make update`.
    - Downloads the latest Czech Wikidata lexemes to `data/wikidata/wikidata_cs_lexemes.txt`.
    - Downloads both [source](https://github.com/plin/slovnik/tree/master/src)
    and [generated](https://github.com/plin/slovnik/tree/master/generated) data of the Czech morphological
    dictionary to the `data/slovnik/generated` and `data/slovnik/src` directories.
    - Updates Hunspell dictionaries in the `hunspell` directory (both munched and unmunched version).

Alternatively if you have [`podman`](https://podman.io/) installed, you can run
`make update_in_podman` to create the dictionary in a container environment
without having any other dependencies installed.

License
-------
Licensed under the [Creative Commons CC0 License](https://creativecommons.org/publicdomain/zero/1.0/).
