#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas
import icu
import subprocess
import hunspell
import os

hunspell_dic_output = "hunspell/cs_CZ.dic"
unmunched_hunspell_dic_output = "hunspell/cs_CZ_unmunched.dic"

hunspell_aff = "hunspell/cs_CZ.aff"
wikidata_lexemes_file = "data/wikidata/wikidata_cs_lexemes.txt"

# x - few words with undetermined categories in files with verbs
part_of_speech_by_number = {"1": "noun", "2": "adjective", "5": "verb", "x": "verb"}
affixes_for_parts_of_speech = {"noun": ["X", "Z", "L", "M", "K", "C", "F", "O", "G"], "adjective": ["P"]}
affixes_for_genders = {"m": ["X"], "i": ["X", "C", "F", "O", "G"], "f": ["X", "Z", "L"], "n": ["X", "M", "K"]}
variants_of_cases = {"O": {"pl6": ["pl6a", "pl6b"]}}
positive_verbs_starting_with_ne = {"negovat", "nechat", "nechávat", "nervovat", "nervóznět", "nést", "neutralizovat"}
verb_forms_impossible_to_negate = {"jest", "je", "se", "si", "by", "bych", "bychom", "bys", "byste"}

# language styles Common Czech, archaism, dialect, expressive, pejorative, profanity, slang
wikidata_excluded_styles = {"Q677531", "Q181970", "Q33384", "Q13408765", "Q545779", "Q184439", "Q8102"}

use_morphological_dictionary_data = True
morpho_dict_files = [
    {
        "name": "data/slovnik/generated/k12.txt",
        "blacklist_file": "data/blacklist_generated_k12.txt",
        "blacklist_columns": 0,
        "use_wikidata_exclusively": True
    },
    {
        "name": "data/slovnik/generated/k5-imp.txt",
        "blacklist_file": "data/blacklist_generated_k5-imp.txt",
        "blacklist_columns": 1,
    },
    {
        "name": "data/slovnik/generated/k5-min.txt",
    },
    {
        "name": "data/slovnik/generated/k5-prit.txt",
    },
    {
        "name": "data/slovnik/generated/k5-trp.txt",
    },
    {
        "name": "data/slovnik/src/k12.txt",
        "rows_limit": 7400,
        "blacklist_file": "data/blacklist_src_k12.txt",
        "blacklist_columns": 0,
    },
] if use_morphological_dictionary_data else []
default_morpho_dict_blacklist = {
    "blacklist_file": "data/blacklist_generated_k5.txt",
    "blacklist_columns": 0,
}

def form_with_affix_and_part_of_speech(form, affix, part_of_speech):
    return f"{form}/{affix} po:{part_of_speech}"

def apply_additional_tags(tags, affix):
    if not affix in variants_of_cases:
        return tags
    additional_tags = []
    for t in range(len(tags) - 1, -1, -1):
        if tags[t] in variants_of_cases[affix]:
            additional_tags += variants_of_cases[affix][tags[t]]
            del tags[t]
    return tags + additional_tags

def tags_for_noun():
    singular_tags = list(map(lambda case_number: f"sg{case_number}", range(2, 8)))
    plural_tags = list(map(lambda case_number: f"pl{case_number}", range(1, 8)))
    return singular_tags + plural_tags

def tags_for_adjective():
    tags = []
    for g in list(affixes_for_genders.keys()):
        if g != "m":  # this form is already there as lemma
            tags.append(f"{g}sg1")
        tags += list(map(lambda t: f"{g}{t}", tags_for_noun()))
    return tags

def find_affix_for_forms_and_tags(forms, part_of_speech, affixes, tags):
    lemma = forms[0]
    for affix in affixes:
        # recreating Hunspell instance because it's impossible to remove word added by add_dic with affix and tags
        # needed to reopen the files because Hunspell instance cannot be copied deeply
        hunspell_cs = hunspell.HunSpell("hunspell/empty.dic", hunspell_aff)

        # the only way how to add a word with tags is from a file
        f = open("tmp_dic.dic", "w")
        f.write(f"1\n{form_with_affix_and_part_of_speech(lemma, affix, part_of_speech)}")
        f.close()
        hunspell_cs.add_dic("tmp_dic.dic")

        forms_to_test = set([lemma])
        tags_to_use = apply_additional_tags(tags.copy(), affix)
        for tag in tags_to_use:
            forms_to_test.update(list(map(
                lambda form: form.decode("utf-8"),
                hunspell_cs.generate2(lemma, f"is:{tag}")
            )))
        if set(forms) == forms_to_test:
            return affix
    return None

def find_affix(forms, part_of_speech, gender):
    """
    Finds affix (pattern) for given forms.
    forms - list of forms, the first one has to be lemma
    """
    lemma = forms[0]
    if part_of_speech == "noun":
        if gender in affixes_for_genders:
            affixes = affixes_for_genders[gender]
        else:
            print(f'[WARN] Missing or unknown gender {gender} for noun {lemma}.')
            affixes = affixes_for_parts_of_speech[part_of_speech]
        return find_affix_for_forms_and_tags(forms, part_of_speech, affixes, tags_for_noun())
    elif part_of_speech == "adjective":
        if gender in affixes_for_genders:
            affixes = affixes_for_genders[gender]
        else:
            affixes = affixes_for_parts_of_speech[part_of_speech]
        return find_affix_for_forms_and_tags(forms, part_of_speech, affixes, tags_for_adjective())
    else:
        return None

def create_negative_forms(forms, part_of_speech, lemma):
    negative_affix = "N"
    if part_of_speech == "verb":
        negative_forms = []
        starts_with_ne_but_is_positive = lemma in positive_verbs_starting_with_ne
        for form in forms:
            if (not form.startswith("ne") or starts_with_ne_but_is_positive) and (form not in verb_forms_impossible_to_negate):
                negative_forms.append(form_with_affix_and_part_of_speech(form, negative_affix, part_of_speech))
            else:
                negative_forms.append(form)
        return negative_forms
    else:
        return forms

def has_allowed_style(styles):
    """
    Checks if any given style is allowed, i.e. at least one its sense contains only allowed styles.
    styles - string with Wikidata style IDs separated by comma, divided into groups
    for senses separated by semicolon
    """
    for styles_for_sense in styles.split(";"):
        if all((style not in wikidata_excluded_styles) for style in styles_for_sense.split(",")):
            return True
    return False

def prepare_forms_and_styles(forms_unparsed, form_styles_unparsed):
    forms = forms_unparsed.split(";")
    form_styles = form_styles_unparsed.split(";")
    for f in range(len(forms) - 1, -1, -1):
        if (len(forms[f]) == 0) or (not has_allowed_style(form_styles[f])):
            del forms[f]
            del form_styles[f]
    return forms, form_styles

def process_phraseme(phraseme_lemma, phraseme_forms, phraseme_form_styles, words_with_affix, words_without_affix, lemmas):
    """
    Adds words of given phraseme or phrase to lists of words and lemmas, if they are not there yet.
    If the phraseme ends with punctuation, the punctuation is removed and the first character is lowercased.
    Forms with not allowed styles are removed.
    phraseme_lemma - string with phraseme lemma
    phraseme_forms - string with phraseme forms separated by semicolon
    phraseme_form_styles - string with styles for phraseme forms separated by semicolon
    words_with_affix - list of all current word forms using affixes
    words_without_affix - list of all current word forms
    lemmas - Pandas Series of current lemmas
    """
    if any((phraseme_lemma.endswith(punctuation)) for punctuation in [".", "!", "?"]):
        phraseme_lemma = phraseme_lemma.replace(phraseme_lemma[0], phraseme_lemma[0].lower(), 1)
        phraseme_lemma = phraseme_lemma[:-1]
    lemma_words = phraseme_lemma.split(" ")
    for w in range(len(lemma_words) - 1, -1, -1):
        if lemma_words[w] in words_without_affix:
            del lemma_words[w]
    if len(lemma_words) > 0:
        lemmas = pandas.concat([lemmas, pandas.Series(lemma_words)], ignore_index=True)

    forms, form_styles = prepare_forms_and_styles(phraseme_forms, phraseme_form_styles)
    form_words = []
    for form in forms:
        form_words += form.split(" ")
    for w in range(len(form_words) - 1, -1, -1):
        if form_words[w] in words_without_affix:
            del form_words[w]
    if len(lemma_words) > 0 or len(form_words) > 0:
        words_with_affix += lemma_words + form_words
        words_without_affix += lemma_words + form_words

def process_wikidata(wikidata_lexemes_file):
    print(f'Processing {wikidata_lexemes_file}.')
    words = pandas.read_csv(wikidata_lexemes_file, sep=":", header=0, keep_default_na=False)

    words_with_affix = []
    words_without_affix = []

    wikidata_words_count = 0
    wikidata_forms_count = 0

    phraseme_indexes = []
    for w in range(len(words.index)):
        if not has_allowed_style(words["style"][w]):
            continue

        part_of_speech = words["po"][w] if len(words["po"][w]) > 0 else None
        if part_of_speech in ["prefix", "suffix", "adjectival suffix"]:
            continue

        if part_of_speech in ["phrase", "phraseme"]:
            phraseme_indexes.append(w)
            continue

        forms, form_styles = prepare_forms_and_styles(words["forms"][w], words["form_styles"][w])

        wikidata_words_count += 1
        wikidata_forms_count += len(forms)

        lemma = words["lemma"][w]
        if len(forms) == 0:  # only lemma available
            words_with_affix += create_negative_forms([lemma], part_of_speech, lemma)
        else:
            if lemma in forms:
                lemma_index = forms.index(lemma)
                if lemma_index != 0:
                    del forms[lemma_index]
                    forms.insert(0, lemma)
            gender = words["gender_aspect"][w] if words["gender_aspect"][w] != "" else None
            affix = find_affix(forms, part_of_speech, gender)
            if affix is not None:
                words_with_affix.append(form_with_affix_and_part_of_speech(lemma, affix, part_of_speech))
            else:
                forms.append(lemma)
                words_with_affix += create_negative_forms(forms, part_of_speech, lemma)
            words_without_affix += forms

    phrasemes = words.loc[phraseme_indexes]
    lemmas = words["lemma"]
    for index, phraseme in phrasemes.iterrows():
        process_phraseme(phraseme["lemma"], phraseme["forms"], phraseme["form_styles"], words_with_affix, words_without_affix, lemmas)

    print(f'{wikidata_lexemes_file}:\t{wikidata_forms_count} forms of {wikidata_words_count} lemmas.')
    return words_with_affix, words_without_affix, lemmas

def process_morphological_dictionary_file(file, blacklist_file, blacklist_columns, use_wikidata_exclusively, wikidata_lemmas):
    print(f'Processing {file}.')

    words_with_affix = []
    words_without_affix = []

    forms = []
    forms_metadata = []

    words_from_file = pandas.read_csv(file, sep=":", header=None)
    total_words_in_file = len(words_from_file)

    words_blacklist = pandas.read_csv(blacklist_file, header=None)[0]
    # apply blacklist and do not include nouns and adjectives whose lemmas are available at Wikidata
    words_from_file = words_from_file[
        (~words_from_file[blacklist_columns].isin(words_blacklist))
        & ((~words_from_file[0].isin(wikidata_lemmas)) | (not use_wikidata_exclusively))
        ]

    row_indexes = words_from_file[0].index.values.tolist()
    forms_used_count = 0
    for ri in range(len(row_indexes)):
        w = row_indexes[ri]
        forms.append(words_from_file[1][w])
        forms_metadata.append(words_from_file[2][w])
        if (ri == len(row_indexes) - 1) or (words_from_file[0][row_indexes[ri + 1]] != words_from_file[0][w]):
            # lemma has to be the first in the list of the forms for find_affix_for_forms
            # determined by the column with metadata because the first column has in a few cases different lemma that it is in the second
            lemma_index = 0
            for fm in forms_metadata:
                if "nSc1" in fm or "k5mF" in fm:  # first case of singular or infinitive for verbs
                    lemma_index = forms_metadata.index(fm)
                    break
            # in some cases (e.g. imperatives for verbs), lemma is not within the list of forms,
            # but in the first columns -> take it for deriving of negations
            # then incorrect lemma is used in find_affix_for_forms, but does not matter for verbs,
            # because they do not have patterns now (excepting the negation)
            if lemma_index == 0:
                lemma = words_from_file[0][w]
            else:
                lemma = forms[lemma_index]
                del forms[lemma_index]
                forms.insert(0, lemma)

            part_of_speech = part_of_speech_by_number[words_from_file[2][w][1]]
            gender = forms_metadata[0][3].lower() if part_of_speech == "noun" else None
            affix = find_affix(forms, part_of_speech, gender)
            if affix is not None:
                words_with_affix.append(form_with_affix_and_part_of_speech(forms[0], affix, part_of_speech))
            else:
                words_with_affix += create_negative_forms(forms, part_of_speech, lemma)

            forms_used_count += len(forms)
            words_without_affix += forms
            forms = []
            forms_metadata = []

        if w % 2e4 == 0:
            print(f'processed {w} of {total_words_in_file} words')

    print(f'{file}:\tused {forms_used_count} forms of {total_words_in_file} words.')
    return words_with_affix, words_without_affix

def process_limited_morphological_dictionary_file(file, blacklist_file, limit):
    print(f'Processing {file} limited to {limit} rows.')  # with most frequent and relevant words
    words_src = pandas.read_csv(file, sep=":", header=None, nrows=limit)[0]
    words_blacklist = pandas.read_csv(blacklist_file, header=None)[0]
    return words_src[~words_src.isin(words_blacklist)]

def write_hunspell(words, file_name):
    sorted_words = sorted(words, key=icu.Collator.createInstance(icu.Locale("cs_CZ.UTF-8")).getSortKey)
    unique_words = pandas.Series(sorted_words).drop_duplicates()
    f = open(file_name, "w")
    f.write(str(len(unique_words)) + "\n")
    unique_words.to_csv(f, index=False, header=False)
    f.close()

def execute(process_args, output_encoding="utf-8"):
    output = subprocess.check_output(process_args).decode(output_encoding).split("\n")
    return output[0:(len(output) - 1)]  # last empty line

def convert_file_to_iso8859_2(file_name, new_file_name, is_affix_file):
    f = open(file_name, "r")
    file_content = f.read()
    f.close()

    if is_affix_file:
        file_content = file_content.replace("SET UTF-8", "SET ISO8859-2")

    f = open(new_file_name, "wb")
    f.write(file_content.encode('iso8859_2'))
    f.close()

def unmunched(source_dictionary, source_affixes, method="unmunch"):
    unmunched_words = []
    if method == "unmunch":
        # munch/unmunch is buggy with some affixes if UTF-8 is used -> unmunch in ISO 8859-2
        tmp_source_dictionary = source_dictionary + ".iso8859_2"
        tmp_source_affixes = source_affixes + ".iso8859_2"
        convert_file_to_iso8859_2(source_dictionary, tmp_source_dictionary, False)
        convert_file_to_iso8859_2(source_affixes, tmp_source_affixes, True)

        unmunched_words = execute(["unmunch", tmp_source_dictionary, tmp_source_affixes], "iso8859_2")

        os.remove(tmp_source_dictionary)
        os.remove(tmp_source_affixes)
    elif method == "wordforms":
        # wordforms for unmunching has to be used one by one word
        for word in pandas.read_csv(source_dictionary, sep=";", header=None, skiprows=1)[0]:  # first line is lines count
            if "/" in word:
                word = word.split("/")[0]
                unmunched_words += execute(["wordforms", source_affixes, source_dictionary, word])
                unmunched_words.append(word)
            else:
                unmunched_words.append(word)
    else:
        raise Exception(f'Unknown unmunch method {method}.')
    return unmunched_words

wikidata_words_with_affix, wikidata_words_without_affix, wikidata_lemmas = process_wikidata(wikidata_lexemes_file)
all_words = pandas.Series(wikidata_words_with_affix)

all_words_without_affix = wikidata_words_without_affix

for file in morpho_dict_files:
    file_name = file["name"]
    blacklist = file["blacklist_file"] if "blacklist_file" in file else default_morpho_dict_blacklist["blacklist_file"]
    blacklist_cols = file["blacklist_columns"] if "blacklist_columns" in file else default_morpho_dict_blacklist["blacklist_columns"]
    use_wikidata_exclusively = file["use_wikidata_exclusively"] if "use_wikidata_exclusively" in file else False
    if "rows_limit" not in file:
        dict_words_with_affix, dict_words_without_affix = process_morphological_dictionary_file(
            file_name, blacklist, blacklist_cols, use_wikidata_exclusively, wikidata_lemmas
        )
        dict_words = pandas.Series(dict_words_with_affix)
        new_words = dict_words[~dict_words.isin(all_words)]
        all_words = pandas.concat([all_words, new_words], ignore_index=True)
        all_words_without_affix += dict_words_without_affix
    else:
        limit = file["rows_limit"]
        dict_words = process_limited_morphological_dictionary_file(file_name, blacklist, limit)
        new_words = dict_words[~dict_words.isin(all_words_without_affix)]
        print(f'{file}:\tused {len(new_words)} new words of {limit} limit.')
        all_words = pandas.concat([all_words, new_words], ignore_index=True)

write_hunspell(all_words.to_list(), hunspell_dic_output)
write_hunspell(unmunched(hunspell_dic_output, hunspell_aff), unmunched_hunspell_dic_output)
os.remove("tmp_dic.dic")
