REPO_ROOT=$(PWD)
DICT_SRC=$(REPO_ROOT)/hunspell
DIST=$(REPO_ROOT)/dist
MORPHO_LOCAL_REPO=$(REPO_ROOT)/data/slovnik
EXTENSIONS_SRC=$(REPO_ROOT)/software-extensions
LO_TARGET=$(DIST)/libreoffice.oxt
MZL_TARGET=$(DIST)/mozilla.xpi

MKDIR=mkdir -p
ZIP=zip -r
RM=rm -rf

.PHONY: prepare_debian
prepare_debian:
	apt-get update
	apt-get install -y python3 hunspell hunspell-tools libhunspell-dev git git-lfs zip

.PHONY: prepare_python
prepare_python:
	pip3 install --user -r requirements.txt

.PHONY: update
update:
	cd data/wikidata && python3 -u get_cs_lexemes.py && python3 -u process_cs_lexemes.py
	git clone https://github.com/plin/slovnik.git $(MORPHO_LOCAL_REPO) || ( cd $(MORPHO_LOCAL_REPO) && git pull )
	python3 -u create_hunspell.py

.PHONY: update_in_podman
update_in_podman:
	bash ./scripts/run_in_podman.sh 'make prepare_debian && make prepare_python && make update'

.PHONY: extensions
extensions :: clean_extensions libreoffice mozilla

.PHONY: extensions_in_podman
extensions_in_podman:
	bash ./scripts/run_in_podman.sh 'make prepare_debian && make extensions'

.PHONY: libreoffice
libreoffice:
	$(MKDIR) $(DIST)
	cd $(DICT_SRC) && $(ZIP) $(LO_TARGET) cs_CZ.aff cs_CZ.dic
	cd $(EXTENSIONS_SRC)/libreoffice && cp description.xml .description.xml.bak
	cd $(EXTENSIONS_SRC)/libreoffice && sed -i -E 's/(<version\s+value=")(.*)("\s*\/>)/\1$(shell date +'%Y.%m.%d.%H%M')\3/' description.xml
	cd $(EXTENSIONS_SRC)/libreoffice && $(ZIP) $(LO_TARGET) ./*
	cd $(EXTENSIONS_SRC)/libreoffice && mv .description.xml.bak description.xml

.PHONY: mozilla
mozilla:
	$(MKDIR) $(DIST)
	cd $(DICT_SRC) && $(ZIP) $(MZL_TARGET) cs_CZ.aff cs_CZ.dic
	cd $(EXTENSIONS_SRC)/mozilla && cp manifest.json .manifest.json.bak
	cd $(EXTENSIONS_SRC)/mozilla && sed -i -E 's/("version":\s+")(.*)("\s*,)/\1\2$(shell date +'.%Y.1%m%d.1%H%M')\3/' manifest.json
	cd $(EXTENSIONS_SRC)/mozilla && $(ZIP) $(MZL_TARGET) ./*
	cd $(EXTENSIONS_SRC)/mozilla && mv .manifest.json.bak manifest.json

.PHONY: clean_extensions
clean_extensions:
	$(RM) $(DIST)
