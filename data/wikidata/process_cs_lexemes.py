#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re
import pandas

input_file_name = "wikidata_cs_lexemes_raw.txt"
output_file_name = "wikidata_cs_lexemes.txt"

def split_to_words(lemma_value, lexeme_category):
    # keep phrases and phrasemes unprocessed
    if lexeme_category in ["phrase", "phraseme"]:
        return [lemma_value]
    if lexeme_category == "proper noun":
        # "t." is "též" in Czech municipality names
        split = re.split(" |-|\.|t\.", lemma_value)
        # exclude numerals (including Roman) from district names
        return [word for word in split if word != "" and not word.isdigit() and not re.fullmatch("[IVXL]+", word)]
    return lemma_value.split(" ")

lexemes = pandas.read_csv(input_file_name, sep=":", header=0, keep_default_na=False)
for lexeme_index, lexeme in lexemes.iterrows():
    if (lexeme_index % 1e3 == 0):
        print(f'Processing lexeme: {lexeme_index} of {len(lexemes.index)}')

    lexeme_lemmas = split_to_words(lexeme["lemma"], lexeme["po"])
    forms = lexeme["forms"].split(";")
    lexeme_forms = [""] * len(lexeme_lemmas)
    for form in forms:
        i = 0

        forms_split = split_to_words(form, lexeme["po"])

        if len(forms_split) > len(lexeme_lemmas):
            print(f'[WARN] Ignored invalid form {form} of lexeme {lexeme_index}.')
            continue
        for form_split in forms_split:
            if len(lexeme_forms[i]) > 0:
                lexeme_forms[i] += ";"
            lexeme_forms[i] += form_split
            i += 1

    output_data = {}
    output_data["lemma"] = lexeme_lemmas
    output_data["po"] = len(lexeme_lemmas) * [lexeme["po"]]
    output_data["gender_aspect"] = len(lexeme_lemmas) * [lexeme["gender_aspect"]]
    output_data["style"] = len(lexeme_lemmas) * [lexeme["style"]]
    output_data["forms"] = lexeme_forms
    output_data["form_styles"] = len(lexeme_lemmas) * [lexeme["form_styles"]]
    data_frame = pandas.DataFrame(output_data)
    if (lexeme_index == 0):
        # first line - write with header
        data_frame.to_csv(output_file_name, sep=":", index=False, header=True, mode='w')
    else:
        # next lines - append
        data_frame.to_csv(output_file_name, sep=":", index=False, header=False, mode='a')
