The extension contains Czech spell check dictionary for LibreOffice.

The dictionary uses data from the Czech morphological dictionary created
by the Masaryk University in Brno: https://github.com/plin/slovnik
and from Czech Wikidata lexemes:
https://www.wikidata.org/wiki/Wikidata:Lexicographical_data

Repository of this dictionary can be found at:
https://gitlab.com/strepon/czech-cc0-dictionaries

The dictionary is considered as experimental (many words missing),
with a lot of space for improvements.

For more details, see: http://ceskeslovniky.cz

How to contribute
-----------------
New words and their forms can be added by adding them to Wikidata:
1. Make sure that the word to be added is not already there,
e.g. by searching at the Ordia tools page: https://tools.wmflabs.org/ordia/

2. Create a new lexeme: https://www.wikidata.org/wiki/Special:NewLexeme
or use friendlier templates for some Czech parts of speech:
https://tools.wmflabs.org/lexeme-forms/
If a new form for an already existing lemma is needed, edit the existing lexeme page.

License
-------
The extension is licensed under the Creative Commons CC0 License:
https://creativecommons.org/publicdomain/zero/1.0/
